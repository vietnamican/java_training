
package s6_algorithm;

import java.util.Scanner;

/**
 *
 * @author vietn
 */
public class Problem1 {
    public static boolean check(Character character){
        return (character == 'a' || character =='i'||character =='u'|| character =='e'||character =='o');
    }
    public static int lengthOfSubstring(String content){
        for(int i=0;i<content.length();i++){
            if(!check(content.charAt(i))){
                return i;
            }
        }
        return content.length();
    }
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String content = scanner.nextLine();
        int maxLength = 0;
        for(int i=0;i<content.length();i++){
            int lengthOfSubString=0;
            if(check(content.charAt(i))){
                lengthOfSubString = lengthOfSubstring(content.substring(i));
            }
            if(lengthOfSubString>maxLength){
                maxLength = lengthOfSubString;
            }
            i+=lengthOfSubString;
        }
        System.out.println(maxLength);
    }
}
