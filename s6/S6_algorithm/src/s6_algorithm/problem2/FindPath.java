
package s6_algorithm.problem2;

import java.util.List;

/**
 *
 * @author vietn
 */
public interface FindPath {
    public boolean findPath(Point start,Point end,List<Point> path,Grid grid);
}
