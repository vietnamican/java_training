package s6_algorithm.problem2;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 *
 * @author vietn
 */
public class FindPathByStack implements FindPath {

    Stack<Point> stack;
    Queue<Point> queue;

    public FindPathByStack() {
        stack = new Stack<>();
        queue = new ArrayDeque<>();
    }

    public Stack<Point> getStack() {
        return stack;
    }

    public void setStack(Stack<Point> stack) {
        this.stack = stack;
    }

    public Queue<Point> getQueue() {
        return queue;
    }

    public void setQueue(Queue<Point> queue) {
        this.queue = queue;
    }

    @Override
    public boolean findPath(Point start, Point end, List<Point> path, Grid grid) {

        stack.push(start);
        while (stack.size() != 0) {
            Point temp;
            temp = stack.pop();
            queue.offer(temp);
            if (temp == end) {
                printPath(start, end, path, grid);
                return true;
            }
            int tempX = temp.getX();
            int tempY = temp.getY();
            int nextX = tempX - 1;
            int nextY = tempY;
            System.out.println(temp.toString());
            if (grid.check(nextX, nextY)) {
                Point nextPoint = grid.grid[nextX][nextY];
                if (queue.contains(nextPoint)) {

                } else {
                    temp.getNext().add(nextPoint);
                    nextPoint.setPre(temp);
                    stack.add(nextPoint);
                }

            }
            nextX = tempX + 1;
            nextY = tempY;
            if (grid.check(nextX, nextY)) {

                Point nextPoint = grid.grid[nextX][nextY];
                if (queue.contains(nextPoint)) {

                } else {
                    temp.getNext().add(nextPoint);
                    nextPoint.setPre(temp);
                    stack.add(nextPoint);
                }
            }
            nextX = tempX;
            nextY = tempY - 1;
            if (grid.check(nextX, nextY)) {
                Point nextPoint = grid.grid[nextX][nextY];
                if (queue.contains(nextPoint)) {

                } else {
                    temp.getNext().add(nextPoint);
                    nextPoint.setPre(temp);
                    stack.add(nextPoint);
                }
            }
            nextX = tempX;
            nextY = tempY + 1;
            if (grid.check(nextX, nextY)) {
                Point nextPoint = grid.grid[nextX][nextY];
                if (queue.contains(nextPoint)) {

                } else {
                    temp.getNext().add(nextPoint);
                    nextPoint.setPre(temp);
                    stack.add(nextPoint);
                }
            }
        }
        printPath(start, end, path, grid);
        return false;
    }

    public static void printPath(Point start, Point end, List<Point> path, Grid grid) {
        Point temp = end;
        List<Point> tempList = new ArrayList<>();
        while (temp != null) {
            tempList.add(0, temp);
            temp = temp.getPre();
        }
        if (tempList.contains(start)) {
            path.addAll(tempList);
        }
    }

}
