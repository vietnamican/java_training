package s6_algorithm.problem2;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author vietn
 */
public class Grid {

    public int height;
    public int width;
    public Point[][] grid;
    public FindPath findPath;

    public Grid(int height, int width) {
        this.height = height;
        this.width = width;
        grid = new Point[height][];
        for (int i = 0; i < height; i++) {
            grid[i] = new Point[width];
            for (int j = 0; j < width; j++) {
                grid[i][j] = new Point();
            }
        }
        findPath = new FindPathByStack();
    }

    public void input(Scanner scanner) {
        for (int i = 0; i < height; i++) {
            String line = scanner.nextLine();
            line = line.replaceAll(" ", "");
//            System.out.println(line);
            for (int j = 0; j < line.length(); j++) {
                grid[i][j] = new Point(i, j, line.charAt(j));
//                System.out.println(grid[i][j].getState());
            }
        }
//        for (int i = 0; i < height; i++) {
//
//            for (int j = 0; j < width; j++) {
//                System.out.print(grid[i][j].getState());
//            }
//            System.out.println("");
//        }
    }

    public boolean check(int x, int y) {

        if (x < 0) {
            return false;
        }
        if (x >= height) {
            return false;
        }
        if (y < 0) {
            return false;
        }
        if (y >= width) {
            return false;
        }
        if (grid[x][y].state == 'T') {
            return false;
        }
        return true;
    }

    public String printPath(Point start, Point end, List<Point> path) {
        String string = "";
        if (path.contains(end)) {
            string = path.toString().replaceAll("[\\[\\]]", "");
        } else {
            string = "NULL";
        }
        return string;

    }

}
