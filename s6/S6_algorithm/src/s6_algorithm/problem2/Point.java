package s6_algorithm.problem2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vietn
 */
public class Point {

    int x;
    int y;
    Character state;
    List<Point> next;
    Point pre;

    public Point() {
    }

    public Point(int x, int y, Character state) {
        this.x = x;
        this.y = y;
        this.state = state;
        next = new ArrayList<Point>();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Character getState() {
        return state;
    }

    public void setState(Character state) {
        this.state = state;
    }

    public List<Point> getNext() {
        return next;
    }

    public void setNext(List<Point> next) {
        this.next = next;
    }

    public Point getPre() {
        return pre;
    }

    public void setPre(Point pre) {
        this.pre = pre;
    }

    public String toString() {
        int x = this.x + 1;
        int y = this.y + 1;
        return "(" + x + ", " + y + ")";
    }
}
