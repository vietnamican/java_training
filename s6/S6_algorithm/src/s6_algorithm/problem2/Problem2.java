package s6_algorithm.problem2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Su dung 2 cach dfs : de quy va stack
 *
 * @author vietn
 */
public class Problem2 {

    public static Grid grid;

    public static Point findStart() {
        for (Point[] points : grid.grid) {
            for (Point point : points) {
                if (point.getState() == 'S') {
                    return point;
                }
            }
        }
        return null;
    }

    public static Point findEnd() {
        for (Point[] points : grid.grid) {
            for (Point point : points) {
                if (point.getState() == 'E') {
                    return point;
                }
            }
        }
        return null;
    }

    public static void findPath(Point start, Point end) {
        List<Point> path = new ArrayList<Point>();
        grid.findPath.findPath(start, end, path, Problem2.grid);
//        System.out.println(path);
        System.out.println(printPath(start, end, path));
    }

    public static String printPath(Point start, Point end, List<Point> path) {
        return grid.printPath(start, end, path);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine();
//        Problem2 problem2 = new Problem2();
        grid = new Grid(n, n);
        grid.input(scanner);
//        System.out.println(grid.grid[0][0].getState());
        Point start = findStart();
//        System.out.println(start.toString());
        Point end = findEnd();
//        System.out.println(end.toString());
        findPath(start, end);
    }
}
