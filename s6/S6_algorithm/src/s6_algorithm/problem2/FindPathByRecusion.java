
package s6_algorithm.problem2;

import java.util.List;

/**
 *
 * @author vietn
 */
public class FindPathByRecusion implements FindPath{

    @Override
    public boolean findPath(Point start, Point end, List<Point> path,Grid grid) {
        if (grid.check(start.getX(), start.getY())) {
            if (path.contains(start)) {
                return false;
            } else {
                path.add(start);
                if (start == end) {
                    return true;
                } else {
                    try {
                        if (findPath(grid.grid[start.getX() - 1][start.getY()], end, path,grid)) {
                            return true;
                        }
                    } catch (Exception e) {

                    }
                    try {
                        if (findPath(grid.grid[start.getX() + 1][start.getY()], end, path,grid)) {
                            return true;
                        }
                    } catch (Exception e) {

                    }
                    try {
                        if (findPath(grid.grid[start.getX()][start.getY() - 1], end, path,grid)) {
                            return true;
                        }
                    } catch (Exception e) {

                    }
                    try {
                        if (findPath(grid.grid[start.getX()][start.getY() + 1], end, path,grid)) {
                            return true;
                        }
                    } catch (Exception e) {

                    }
                    path.remove(start);
                }
            }
        } else {
            return false;
        }
        return false;
    }

    
    
}
